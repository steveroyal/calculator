import 'package:calculator/CalculatorData.dart';
import 'package:flutter/material.dart';
import 'package:calculator/CalculatorFace.dart';
import 'package:calculator/CalculatorControls.dart';
import 'package:provider/provider.dart';

class Calculator extends StatefulWidget{
  @override
  CalculatorState createState() => CalculatorState();
}


class CalculatorState extends State<Calculator>{

  @override
  Widget build(BuildContext context) {

    return MultiProvider(
      providers: [
        ChangeNotifierProvider(builder:(context)=>CalculatorData(),)
      ],
      child:SafeArea(
      child:Scaffold(
      appBar: null,
      body: Container(
        decoration: BoxDecoration(color: Color.fromRGBO(0,0, 0,1)),
        child: Column(
          children: <Widget>[
            Expanded(flex: 1,child:CalculatorFace()),
            Expanded(flex:3,child:CalculatorControls(),),
          ],
        ),
      ),
    )
    )
    );


  }

}