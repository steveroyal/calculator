import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:calculator/CalculatorData.dart';

enum controlTheme { Primary, Function, Numeral }

class CalculatorControls extends StatefulWidget {
  @override
  CalculatorControlsState createState() => CalculatorControlsState();
}

class CustomTheme {
  Color _themeColor;
  Color _hoverColor;
  TextStyle _textStyle;
}

class CalculatorControlsState extends State<CalculatorControls> {

  final List<FlatButton> controls = <FlatButton>[];

  @override
  Widget build(BuildContext context) {

    return ChangeNotifierProvider(
      child: PortraitControls(),
      builder: (_)=>ControlsFunction(),
    );
  }
}

class CustomButton extends StatefulWidget {

  CustomButton({Key key, this.value, this.theme, this.width, this.height});
  final String value;
  final double width, height;
  final controlTheme theme;
  final primaryTheme = Color.fromRGBO(255, 255, 255, .70),
      primaryThemeHover = Color.fromRGBO(255, 255, 255, .80),
      numeralTheme = Color.fromRGBO(255, 255, 255, .15),
      numeralThemeHover = Color.fromRGBO(255, 255, 255, .35),
      functionTheme = Color.fromRGBO(254, 148, 4, .9),
      functionThemeHover = Color.fromRGBO(255, 196, 115, 1);

  @override
  CustomButtonState createState() => CustomButtonState();
}

class CustomButtonState extends State<CustomButton> {
  var customTheme = new CustomTheme();
  bool _tapdown = false;

  @override
  Widget build(BuildContext context) {

      //Get calculator Functions from the provider
    ControlsFunction controlsFunction = Provider.of<ControlsFunction>(context);
    
    _chooseTheme(controlsFunction);

    return SizedBox(
        height: (widget.height == null) ? 60 : widget.height,
        width: (widget.width == null) ? 60 : widget.width,
        child: GestureDetector(
          onTapDown: (e) {
            
            setState(() {
              _tapdown = true;
            });
          },
          onTapCancel:(){
            setState(() {
              _tapdown = false;
            });
          },
          child: FlatButton(
            shape: null,
            padding: EdgeInsets.all(0),
            onPressed: ()=>_calculatorButtonHandler(widget.value,controlsFunction),
            child: Stack(
              children: <Widget>[
                Container(
                  decoration: BoxDecoration(
                      color: (_tapdown == false)
                          ? customTheme._themeColor
                          : customTheme._hoverColor,
                      borderRadius: BorderRadius.all(Radius.circular(35))),
                  child: Center(
                    child: Text(
                      widget.value,
                      style: customTheme._textStyle,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ));
  }

  void _chooseTheme(ControlsFunction controlsFunction) {

    switch (widget.theme) {
      
      case controlTheme.Primary:
        customTheme._themeColor = widget.primaryTheme;
        customTheme._hoverColor = widget.primaryThemeHover;
        customTheme._textStyle = TextStyle(color: Colors.black, fontSize: 20,fontFamily:'Roboto');
        break;

      case controlTheme.Function:

        if(controlsFunction.status && controlsFunction.function==widget.value){
        customTheme._themeColor = Colors.white;
        customTheme._textStyle = TextStyle(color: widget.functionTheme, fontSize: 32,fontFamily:'Raleway');
        }else{
        customTheme._themeColor = widget.functionTheme;
        customTheme._hoverColor = widget.functionThemeHover;
        customTheme._textStyle = TextStyle(color: Colors.white, fontSize: 32,fontFamily:'Raleway');
        }
       
        break;

      case controlTheme.Numeral:
        customTheme._themeColor = widget.numeralTheme;
        customTheme._hoverColor = widget.numeralThemeHover;
        customTheme._textStyle = TextStyle(color: Colors.white, fontSize: 20,fontFamily:'Roboto');
        break;

      default:
        customTheme._themeColor = widget.primaryTheme;
        customTheme._hoverColor = widget.primaryThemeHover;
        customTheme._textStyle = TextStyle(color: Colors.black, fontSize: 20,fontFamily:'Roboto',);
        break;
    }
  }


  void _calculatorButtonHandler(String value,ControlsFunction controlsFunction){

  controlsFunction.setFunction="";
  controlsFunction.setStatus=false;

  switch(value)  {

    case "×":
    controlsFunction.setStatus=true;
    controlsFunction.setFunction=value;
    break;

    case "-":
    controlsFunction.setStatus=true;
    controlsFunction.setFunction=value;
    break;
    
    case "+":
    controlsFunction.setStatus=true;
    controlsFunction.setFunction=value;
    break;

    case "÷":
    controlsFunction.setStatus=true;
    controlsFunction.setFunction=value;
    break;



  }


  }


}

class PortraitControls extends StatefulWidget {

  @override 
  PortraitControlStates createState() => PortraitControlStates();

}

class PortraitControlStates extends State<PortraitControls>{

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.only(top: 10),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                CustomButton(
                  value: "AC",
                  key: Key("acBtn"),
                ),
                CustomButton(
                  value: "+/-",
                  key: Key("signBtn"),
                ),
                CustomButton(
                  value: "%",
                  key: Key("percentageBtn"),
                ),
                CustomButton(
                  value: "÷",
                  key: Key("divisionBtn"),
                  theme: controlTheme.Function,
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                CustomButton(
                  value: "7",
                  key: Key("num7Btn"),
                  theme: controlTheme.Numeral,
                ),
                CustomButton(
                  value: "8",
                  key: Key("num8Btn"),
                  theme: controlTheme.Numeral,
                ),
                CustomButton(
                  value: "9",
                  key: Key("num9Btn"),
                  theme: controlTheme.Numeral,
                ),
                CustomButton(
                  value: "×",
                  key: Key("multipyBtn"),
                  theme: controlTheme.Function,
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                CustomButton(
                  value: "4",
                  key: Key("num4Btn"),
                  theme: controlTheme.Numeral,
                ),
                CustomButton(
                  value: "5",
                  key: Key("num5Btn"),
                  theme: controlTheme.Numeral,
                ),
                CustomButton(
                  value: "6",
                  key: Key("num6Btn"),
                  theme: controlTheme.Numeral,
                ),
                CustomButton(
                  value: "-",
                  key: Key("subBtn"),
                  theme: controlTheme.Function,
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                CustomButton(
                  value: "1",
                  key: Key("num1Btn"),
                  theme: controlTheme.Numeral,
                ),
                CustomButton(
                  value: "2",
                  key: Key("num2Btn"),
                  theme: controlTheme.Numeral,
                ),
                CustomButton(
                  value: "3",
                  key: Key("num3Btn"),
                  theme: controlTheme.Numeral,
                ),
                CustomButton(
                  value: "+",
                  key: Key("addBtn"),
                  theme: controlTheme.Function,
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                CustomButton(
                  value: "0",
                  key: Key("num0Btn"),
                  theme: controlTheme.Numeral,
                  width: 140,
                  height: 60,
                ),
                CustomButton(
                  value: ".",
                  key: Key("pointBtn"),
                  theme: controlTheme.Numeral,
                ),
                CustomButton(
                  value: "=",
                  key: Key("equalBtn"),
                  theme: controlTheme.Function,
                ),
              ],
            )
          ],
        ));
  }

}
