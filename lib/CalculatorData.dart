import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';



class CalculatorData with ChangeNotifier{

String _screenVal="";
List<String> _history = [];

  String get screenval => _screenVal;
  List<String> get history  => _history;

  set setScreenValue(String sv){
    _screenVal = sv;
    notifyListeners();
  }

  set setHistory(List<String> h){
    _history = h;
    notifyListeners();
  }

  }

  class ControlsFunction with ChangeNotifier{  

    bool _status=false;
    String _function;

    bool get status  => _status;
    String get function =>_function;

    set setStatus(bool s){
      _status=s;
      notifyListeners();
    }

     set setFunction(String f){
      _function=f;
      notifyListeners();
    }





  }







