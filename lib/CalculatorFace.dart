import 'package:calculator/CalculatorData.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';



class CalculatorFace extends StatefulWidget{


  @override
  CalculatorFaceState createState() => CalculatorFaceState();
}


class CalculatorFaceState extends State<CalculatorFace>{

  bool _dragged = false;

  @override
  Widget build(BuildContext context)
  {

    //Get calculator data from the provider
    CalculatorData calculatorData = Provider.of<CalculatorData>(context);

    return Container(
    child:Column(children: <Widget>[
      Expanded(
        child: Container(
        child:History()
        ),
        ),
        Expanded(
        child:Container(
          width: double.infinity,
          child:GestureDetector(
          onHorizontalDragUpdate: (e){
          
         /** Allow user to only delete one key per swipe by setting _dragged to true on first drag and set it back to false
          *  when drag finish and user remove finger from the screen*/
          
          if(_dragged==false && calculatorData.screenval.length!=0){
           _dragged=true; 
           calculatorData.setScreenValue=calculatorData.screenval.substring(0,(calculatorData.screenval.length-1));
          }

          },
          onHorizontalDragEnd: (e){
           _dragged=false; 
          },
          child:SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          reverse: true,
          physics: const NeverScrollableScrollPhysics(),
          child:Align(
            widthFactor: 5,
            alignment: Alignment.bottomRight,
           child:Padding(
             padding: EdgeInsets.only(right: 5),
             child:Text(calculatorData.screenval,textDirection:TextDirection.ltr,style: TextStyle(color: Colors.white,fontSize: 45,fontFamily:'Roboto',)),
          )
          ),
        ),
        )
        )
        )
    ],),
    );
  }
}



//History StatefulWidget to Manage Calculation History
class History extends StatefulWidget{

 @override
 HistoryState createState() => HistoryState();

}

class HistoryState extends State<History>{

  @override
  Widget build(BuildContext context){

    CalculatorData calculatorData = Provider.of<CalculatorData>(context);

    //List build to build a list views from the Calculation history
    return ListView.builder(
      //Get the Length of the calculation history list
      itemCount: calculatorData.history.length,
      //Build a Widget for each of the item in the history list
      itemBuilder: (BuildContext context,int index){

      //Gesture Detector to detect when user tap on this history item
       return GestureDetector(
         onTap: (){
           //Set the Screen value to the value of the history item tap
          calculatorData.setScreenValue=calculatorData.history[index].toString();
         },
         child:Padding(
           padding: EdgeInsets.all(5),
           child:Align(
           alignment: Alignment.topRight,
           //Show the history value as Text
           child: Text(calculatorData.history[index].toString(),style: TextStyle(fontSize: 18,color: Color.fromRGBO(255, 255, 255,.6)),),
       )
       )
       );
      },
    );

  }


}